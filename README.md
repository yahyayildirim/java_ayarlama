# java_ayarlama
Öncelikle bu scriptleri kişisel kullanım için hazırlıyorum. Bu sebeple; kod bütünlüğü olmayacaktır. Eleştirmekten ziyade tavsiyelere her zaman açık olduğumu belirtmek istiyorum.

# Bu Script İle;
* Java ve türevlerine yapılan tüm bağlar silinecek ve eğer sisteminizde oracle-java8-jre veya oracle-java8-jdk paketi yüklü ise yeniden configure edilecektir.
* Eğer java yüklü değilse depodan oracle-java8-jre indirilerek kurulacak ve gerekli tüm linklemeleri yapacaktır.
* Java ile ilgili bir kaç ayarıda deployment.properties dosyasına yazarak ayarlamaları otomatik yapacaktır.
* Java Güvenli siteler bölümüne kesenek.sgk.gov.tr otomatik olarak eklenecektir.
* /usr/share/applications/ dizinine iki adet kısayol oluşturacaktır.
* Java 8 Web Start ve Java Ayarları uygulama menüsünden erişilebilir olacaktır.

Yükleme tamamlandıktan sonra scriptin sonunda yapılması tavsiye edilenleri yapmayı unutmayınız...

# Kurulum
### Uçbirim açın ve aşağıdaki kodları sırayla uygulayın.

```wget -nc O- https://gitlab.com/yahyayildirim/java_ayarlama/-/raw/main/java_ayarlama.sh```

```sudo bash java_ayarlama.sh```

