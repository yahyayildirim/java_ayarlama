#!/usr/bin/env bash

#############################################
# Oracle Java 8 Kurulumu ve Ayarlaması      #
# Pardus 19 Sürümleri için hazırlanmıştır.  #
# Hazırlayan: Yahya YILDIRIM                #
# http://github.com/yahyayildirim			#
# https://kod.pardus.org.tr/yahyayildirim   #
#############################################

kirmizi=$'\e[1;31m';
turkuaz=$'\e[1;36m';
yesil=$'\e[1;32m';
sifirla=$'\e[0m';
kalin=$'\e[1m';
baslik="${kirmizi}###  JAVA KURULUM VE AYARLAMA SCRİPTİNE HOŞ GELDİNİZ  ###${sifirla}"

echo -e "$baslik"

tarih () {
	date '+%d-%m-%Y %H:%M:%S'
}

if [[ "$(whoami)" != root ]]; then
	echo "${kalin}Lütfen scripti ${kirmizi}sudo${sifirla} ${kalin}yetkisi ile çalıştırın...${sifirla}"
	echo "${kalin}Örnek: ${turkuaz}sudo ./`basename $0`${sifirla} ${kalin}veya${sifirla} ${kalin}${turkuaz}sudo $0${sifirla}"
	exit 0
fi

kullanici_home=$(eval echo "~${SUDO_USER}")
kullanici_name=$(echo $kullanici_home | sed 's|.*/||')
kullanici_grup=$(id -gn $kullanici_name) 

if [[ `echo $(apt show icedtea-netx 2>/dev/null | grep -w "APT-Manual-Installed: yes")` ]]; then
    echo -e "${kirmizi}$(tarih) >>> İceTea Java Plugin Kaldırılıyor....${sifirla}"
    apt remove --purge icedtea-netx -y > /dev/null 2>&1
fi


# JAVA KURULU İSE YAPILANDIRMA DOSYALARI YENİLENECEK, DEĞİL İSE YENİ KURULUM YAPACAK

if [[ `echo $(apt show oracle-java8-jre 2>/dev/null | grep -w "APT-Manual-Installed: yes")` ]]; then
	echo -e "${kalin}$(tarih) >>> Oracle java8 jre sisteminizde kurulu olduğu için tüm ayarları sıfırlanıyor...${sifirla}"
	set java javaws keytool orbd pack200 rmid rmiregistry servertool tnameserv unpack200 policytool ControlPanel jcontrol
	for A;
	do
		sudo update-alternatives --remove-all "$A" > /dev/null 2>&1
		# echo "$A için yeni yapılandırma ayarları tanımlanıyor..."; sleep 2
		# sudo update-alternatives --install /usr/bin/$A $A /usr/lib/jvm/oracle-java8-jre-amd64/bin/$A 317
		# echo
	done
	echo "${yesil}$(tarih) >>> Sıfırlanan/silinen yapılandırma dosyaları yeniden oluşturuldu.${sifirla}"
	dpkg-reconfigure oracle-java8-jre > /dev/null 2>&1

elif [[ `echo $(apt show oracle-java8-jdk 2>/dev/null | grep -w "APT-Manual-Installed: yes")` ]]; then
	echo -e "${kalin}$(tarih) >>> Oracle java8 jdk sisteminizde kurulu olduğu için tüm ayarları sıfırlanıyor...${sifirla}"
	set java javaws keytool orbd pack200 rmid rmiregistry servertool tnameserv unpack200 policytool ControlPanel jcontrol \
	appletviewer extcheck idlj jar jarsigner javac javadoc javah javap jcmd jconsole jdb jdeps jhat jinfo jmap jmc jps \
	jrunscript jsadebugd jstack jstat jstatd jvisualvm native2ascii rmic schemagen serialver wsgen wsimport xjc
	for A;
	do
		sudo update-alternatives --remove-all "$A" > /dev/null 2>&1
		# echo "$A için yeni yapılandırma ayarları tanımlanıyor..."; sleep 2
		# sudo update-alternatives --install /usr/bin/$A $A /usr/lib/jvm/oracle-java8-jdk-amd64/bin/$A 317
		# echo
	done
	echo "${yesil}$(tarih) >>> Sıfırlanan/silinen yapılandırma dosyaları yeniden oluşturuldu.${sifirla}"
	dpkg-reconfigure oracle-java8-jdk > /dev/null 2>&1

else
	echo -e "${yesil}$(tarih) >>> Oracle Java8 jre Kuruluyor. Lütfen bekleyin...${sifirla}"
	set java javaws keytool orbd pack200 rmid rmiregistry servertool tnameserv unpack200 policytool ControlPanel jcontrol
	for A;
	do
		sudo update-alternatives --remove-all "$A" > /dev/null 2>&1
		# echo "$A için yeni yapılandırma ayarları tanımlanıyor..."; sleep 2
		# sudo update-alternatives --install /usr/bin/$A $A /usr/lib/jvm/oracle-java8-jre-amd64/bin/$A 317
		# echo
	done
	apt -f install --reinstall oracle-java8-jre
fi


# JAVA KURAL TANIMLAMA VE GÜVENLİ SİTE EKLEME
JAVA_CONF=$kullanici_home/.java/
JAVA_DEPL=$kullanici_home/.java/deployment
JAVA_SECR=$kullanici_home/.java/deployment/security

if [[ -d $JAVA_DEPL ]]; then
	echo -e "${kalin}$(tarih) >>> Java ayarları deployment.properties dosyasına yazıldı.${sifirla}"
	tee > $JAVA_DEPL/deployment.properties << EOF
deployment.proxy.type=0
deployment.webjava.enabled=true
deployment.cache.enabled=false
deployment.security.level=HIGH
deployment.javaws.update.timeout=0
deployment.browser.path=/usr/bin/firefox
EOF
fi

if [[ -e $JAVA_SECR/exception.sites ]]; then
	sed -i -e '/kesenek/d' -e '/sgk/d' $JAVA_SECR/exception.sites
	echo "https://kesenek.sgk.gov.tr/" >> $JAVA_SECR/exception.sites
	echo "${yesil}$(tarih) >>> Java Güvenli Sitelere https://kesenek.sgk.gov.tr/ eklendi.${sifirla}"
else
	if [[ -d $JAVA_SECR ]]; then
		echo "https://kesenek.sgk.gov.tr/" > $JAVA_SECR/exception.sites
		echo "${yesil}$(tarih) >>> Java Güvenli Sitelere https://kesenek.sgk.gov.tr/ eklendi.${sifirla}"
	else
		mkdir -p -m 755 $JAVA_SECR
		echo "https://kesenek.sgk.gov.tr/" > $JAVA_SECR/exception.sites
		echo "${yesil}$(tarih) >>> Java Güvenli Sitelere https://kesenek.sgk.gov.tr/ eklendi.${sifirla}"
	fi

fi
chown $kullanici_name:"$kullanici_grup" -R $JAVA_CONF

echo "${kalin}$(tarih) >>> Eski uygulama kısayolları silindi... ${sifirla}"
sudo rm -rf /usr/share/applications/java*

echo -e "${turkuaz}$(tarih) >>> Uygulamalar Menüsüne Java 8 Web Start için Kısayol oluşturuldu...${sifirla}"
tee > /usr/share/applications/javaws-viewer.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Name=Java 8 Web Start
Comment=Java 8 Web Start
Exec=/usr/bin/javaws %u
Terminal=false
Type=Application
Icon=java
Categories=Java;Applications;Network;
MimeType=application/x-java-jnlp-file;x-scheme-handler/jnlp;x-scheme-handler/jnlps
EOF
chmod +xr /usr/share/applications/javaws-viewer.desktop

echo -e "${turkuaz}$(tarih) >>> Uygulamalar Menüsüne Java Ayarları için Kısayol oluşturuldu...${sifirla}"
tee > /usr/share/applications/javaws-control.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Name=Java Ayarları
Comment=Java Ayarları
Exec=jcontrol
Terminal=false
Type=Application
Icon=java
Categories=Application;Network;
EOF
chmod +xr /usr/share/applications/javaws-control.desktop

echo -e """${kalin}
####### BENİ OKU ####################
# Kurulum başarılı bir şekilde tamamlanmıştır.
# Java ile ilgili tüm ayarlar otomatik olarak yapılmıştır.
# İndirdiğiniz .jnlp dosyalarını çift tıklayarak direk açabilirsiniz.
# Eğer açılmaz ise indirdiğiniz dosyaya sağ tıklayarak özellikler ve Birlikte Aç
# seçeneğinden Java 8 Web Start seçerek ön tanımlı hale getirmeniz gerekmektedir.
#####################################
${sifirla}"""
